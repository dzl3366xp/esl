function goTop() {
  window.open('home1.html');
}
function goTrade() {
  window.open('trade1.html');
}
function goAssets() {
  window.open('assets1.html');
}
function setClass(_ele, type) {
  $('.item-active').removeClass('item-active');
  $(_ele).addClass('item-active');
  var viewpage = 'home.html';
  if (type == 3) {
    viewpage = 'trade.html';
  } else if (type == 4) {
    viewpage = 'assets.html';
  }
  window.location = viewpage;
}

function openChatbot() {
  $('.imgcli').click();
}

$(function () {
  setInterval(setClock, 100);
});
function setClock() {
  var date = new Date();
  var year = toString(date.getFullYear());
  var month = toString(date.getMonth() + 1); //js从0开始取
  var date1 = toString(date.getDate());
  var hour = toString(date.getHours());
  var minutes = toString(date.getMinutes());
  var second = toString(date.getSeconds());
  var time = year + '/' + month + '/' + date1 + ' ' + hour + ':' + minutes + ':' + second;
  $('#clock').html(time);
}
function toString(value) {
  return (value + '').padStart(2, '0');
}

var currentPosition, timer;
function GoTop() {
  timer = setInterval('runToTop()', 1);
}
function runToTop() {
  currentPosition = document.documentElement.scrollTop || document.body.scrollTop;
  currentPosition -= 10;
  if (currentPosition > 0) {
    window.scrollTo(0, currentPosition);
  } else {
    window.scrollTo(0, 0);
    clearInterval(timer);
  }
}
var currentPosition2, timer2;
function GoBottom() {
  timer2 = setInterval('runBottom()', 1);
}
function runBottom() {
  currentPosition2 = document.documentElement.scrollTop || document.body.scrollTop;
  currentPosition2 += 10;
  if(currentPosition2 > 260) {
    window.scrollTo(0, 0);
    clearInterval(timer2);
  }
  if (currentPosition2 > 0) {
    window.scrollTo(0, currentPosition2);
  }
}
function closeEyes(type){
  debugger
  if (type == 1){
    $("#openEye").css("display","none")
    $("#closeEye").css("display","")
  }
  if (type == 2){
    $("#openEye").css("display","")
    $("#closeEye").css("display","none")
    
  }
  
}
function helpOpen(){
   window.open("video.html")
}
