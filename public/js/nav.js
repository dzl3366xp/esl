function windowLoadInit() {
  setTimeout(function () {
    var $header = jQuery(".page_header").first();
    if ($header.length) {
      menuHideExtraElements();
      var headerHeight = $header.outerHeight();
      $header
        .wrap('<div class="page_header_wrapper"></div>')
        .parent()
        .css({ height: headerHeight }); //wrap header for smooth stick and unstick

      //get offset
      var headerOffset = 0;
      //check for sticked template headers
      headerOffset = $header.offset().top;

      //for boxed layout - show or hide main menu elements if width has been changed on affix
      jQuery($header).on("affixed-top.bs.affix affixed.bs.affix", function (e) {
        if ($header.hasClass("affix-top")) {
          $header
            .parent()
            .removeClass("affix-wrapper affix-bottom-wrapper")
            .addClass("affix-top-wrapper");
        } else if ($header.hasClass("affix")) {
          $header
            .parent()
            .removeClass("affix-top-wrapper affix-bottom-wrapper")
            .addClass("affix-wrapper");
        } else if ($header.hasClass("affix-bottom")) {
          $header
            .parent()
            .removeClass("affix-wrapper affix-top-wrapper")
            .addClass("affix-bottom-wrapper");
        } else {
          $header
            .parent()
            .removeClass(
              "affix-wrapper affix-top-wrapper affix-bottom-wrapper"
            );
        }
        menuHideExtraElements();
      });

      //if header has different height on afixed and affixed-top positions - correcting wrapper height
      jQuery($header).on("affixed-top.bs.affix", function () {
        $header.parent().css({ height: $header.outerHeight() });
      });

      jQuery($header).affix({
        offset: {
          top: headerOffset,
          bottom: 0,
        },
      });
    }
  }, 100);

  //mega menu
  initMegaMenu();
  //aside affix

  jQuery("body").scrollspy("refresh");

  //animation to elements on scroll
  if (jQuery().appear) {
    jQuery(".to_animate").appear();
    jQuery(".to_animate")
      .filter(":appeared")
      .each(function (index) {
        var self = jQuery(this);
        var animationClass = !self.data("animation")
          ? "fadeInUp"
          : self.data("animation");
        var animationDelay = !self.data("delay") ? 210 : self.data("delay");
        setTimeout(function () {
          self.addClass("animated " + animationClass);
        }, index * animationDelay);
      });

    jQuery("body").on("appear", ".to_animate", function (e, $affected) {
      jQuery($affected).each(function (index) {
        var self = jQuery(this);
        var animationClass = !self.data("animation")
          ? "fadeInUp"
          : self.data("animation");
        var animationDelay = !self.data("delay") ? 210 : self.data("delay");
        setTimeout(function () {
          self.addClass("animated " + animationClass);
        }, index * animationDelay);
      });
    });

    //counters init on scroll
    jQuery(".counter").appear();
    jQuery(".counter")
      .filter(":appeared")
      .each(function (index) {
        if (jQuery(this).hasClass("counted")) {
          return;
        } else {
          jQuery(this).countTo().addClass("counted");
        }
      });
    jQuery("body").on("appear", ".counter", function (e, $affected) {
      jQuery($affected).each(function (index) {
        if (jQuery(this).hasClass("counted")) {
          return;
        } else {
          jQuery(this).countTo().addClass("counted");
        }
      });
    });

    //bootstrap animated progressbar
    if (jQuery().progressbar) {
      jQuery(".progress .progress-bar").appear();
      jQuery(".progress .progress-bar")
        .filter(":appeared")
        .each(function (index) {
          jQuery(this).progressbar({
            transition_delay: 300,
          });
        });
      jQuery("body").on(
        "appear",
        ".progress .progress-bar",
        function (e, $affected) {
          jQuery($affected).each(function (index) {
            jQuery(this).progressbar({
              transition_delay: 300,
            });
          });
        }
      );
      //animate progress bar inside bootstrap tab
      jQuery('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
        jQuery(jQuery(e.target).attr("href"))
          .find(".progress .progress-bar")
          .progressbar({
            transition_delay: 300,
          });
      });
    }
  } //appear check

  //flickr
  // use http://idgettr.com/ to find your ID
  if (jQuery().jflickrfeed) {
    var $flickr = jQuery("#flickr");
    if ($flickr.length) {
      if (!$flickr.hasClass("flickr_loaded")) {
        $flickr
          .jflickrfeed(
            {
              flickrbase: "http://api.flickr.com/services/feeds/",
              limit: 8,
              qstrings: {
                id: "131791558@N04",
              },
              itemTemplate:
                '<a href="{{image_b}}" data-gal="prettyPhoto[pp_gal]"><li><img alt="{{title}}" src="{{image_s}}" /></li></a>',
            },
            function (data) {
              $flickr.find("a").prettyPhoto({
                hook: "data-gal",
                theme: "facebook",
              });
            }
          )
          .addClass("flickr_loaded");
      }
    }
  }

  //video images preview
  jQuery(".embed-placeholder").each(function () {
    jQuery(this).on("click", function (e) {
      e.preventDefault();
      jQuery(this).replaceWith(
        '<iframe class="embed-responsive-item" src="' +
          jQuery(this).attr("href") +
          "?rel=0&autoplay=1" +
          '"></iframe>'
      );
    });
  });

  // init Isotope
  jQuery(".isotope_container").each(function (index) {
    var $container = jQuery(this);
    var layoutMode = $container.hasClass("masonry-layout")
      ? "masonry"
      : "fitRows";
    $container.isotope({
      percentPosition: true,
      layoutMode: layoutMode,
      masonry: {},
    });

    var $filters = jQuery(this).attr("data-filters")
      ? jQuery(jQuery(this).attr("data-filters"))
      : $container.prev().find(".filters");
    // bind filter click
    if ($filters.length) {
      $filters.on("click", "a", function (e) {
        e.preventDefault();
        var filterValue = jQuery(this).attr("data-filter");
        $container.isotope({ filter: filterValue });
        jQuery(this).siblings().removeClass("selected active");
        jQuery(this).addClass("selected active");
      });
    }
  });

  //page preloader
  jQuery(".preloaderimg").fadeOut();
  jQuery(".preloader")
    .delay(200)
    .fadeOut("slow")
    .delay(200, function () {
      jQuery(this).remove();
    });
} //eof windowLoadInit

jQuery(document).ready(function () {
  documentReadyInit();
}); //end of "document ready" event

jQuery(window).on("load", function () {
  windowLoadInit();
}); //end of "window load" event

jQuery(window).on("resize", function () {
  jQuery("body").scrollspy("refresh");

  // fixSliderHeight();

  //header processing
  menuHideExtraElements();
  initMegaMenu();
  var $header = jQuery(".page_header").first();
  //checking document scrolling position
  if (
    $header.length &&
    !jQuery(document).scrollTop() &&
    $header.first().data("bs.affix")
  ) {
    $header.first().data("bs.affix").options.offset.top = $header.offset().top;
  }
  jQuery(".page_header_wrapper").css({ height: $header.first().outerHeight() }); //editing header wrapper height for smooth stick and unstick
});

function initMegaMenu() {
  var $megaMenu = jQuery(".mainmenu_wrapper .mega-menu");
  if ($megaMenu.length) {
    var windowWidth = jQuery("body").innerWidth();
    if (windowWidth > 991) {
      $megaMenu.each(function () {
        var $thisMegaMenu = jQuery(this);
        //temporary showing mega menu to propper size calc
        $thisMegaMenu.css({ display: "block", left: "auto" });
        var thisWidth = $thisMegaMenu.outerWidth();
        var thisOffset = $thisMegaMenu.offset().left;
        var thisLeft = thisOffset + thisWidth / 2 - windowWidth / 2;
        $thisMegaMenu.css({ left: -thisLeft, display: "none" });
        if (!$thisMegaMenu.closest("ul").hasClass("nav")) {
          $thisMegaMenu.css("left", "");
        }
      });
    }
  }
}

//hidding menu elements that do not fit in menu width
function menuHideExtraElements() {
  if (!jQuery(".page_header").hasClass("not_hide_menuitems")) {
    jQuery("#more-li").remove();
    var wrapperWidth = jQuery(".sf-menu").width();
    var summaryWidth = 0;
    //get all first level menu items
    var $liElements = jQuery(".sf-menu > li");
    $liElements.removeClass("md-hidden");
    $liElements.each(function (index) {
      var elementWidth = jQuery(this).outerWidth();
      summaryWidth += elementWidth;
      if (summaryWidth >= wrapperWidth) {
        var $newLi = jQuery('<li id="more-li"><a>...</a><ul></ul></li>');
        jQuery($liElements[index - 1]).before($newLi);
        var newLiWidth = jQuery($newLi).outerWidth(true);
        var $extraLiElements = $liElements.filter(":gt(" + (index - 2) + ")");
        $extraLiElements.clone().appendTo($newLi.find("ul"));
        $extraLiElements.addClass("md-hidden");
        return false;
      }
    });
  }
}

function documentReadyInit() {
  ////////////
  //mainmenu//
  ////////////
  if (jQuery().scrollbar) {
    jQuery('[class*="scrollbar-"]').scrollbar();
  }
  if (jQuery().superfish) {
    jQuery("ul.sf-menu").superfish({
      popUpSelector: "ul:not(.mega-menu ul), .mega-menu ",
      delay: 700,
      animation: { opacity: "show", marginTop: 1 },
      animationOut: { opacity: "hide", marginTop: 5 },
      speed: 200,
      speedOut: 200,
      disableHI: false,
      cssArrows: true,
      autoArrows: true,
    });
    jQuery("ul.sf-menu-side").superfish({
      popUpSelector: "ul:not(.mega-menu ul), .mega-menu ",
      delay: 500,
      animation: { opacity: "show", height: 100 + "%" },
      animationOut: { opacity: "hide", height: 0 },
      speed: 400,
      speedOut: 300,
      disableHI: false,
      cssArrows: true,
      autoArrows: true,
    });
  }

  /////////////////////////
  //logo inside main menu//
  /////////////////////////
  var $headerLogoCenter = jQuery(".header_logo_center");
  if ($headerLogoCenter.length) {
    var logoWidth = $headerLogoCenter.find(".logo").width();
    var $menuLis = $headerLogoCenter.find(".sf-menu > li");
    var centerLi = Math.ceil($menuLis.length / 2);
    //you can change return value (padding-left) to any that suits your needs.
    //this works good if you have an even number of main menu items
    $menuLis.eq(centerLi).css("paddingLeft", function () {
      return logoWidth + jQuery(this).outerWidth(true);
    });
  }

  //toggle mobile menu
  jQuery(".toggle_menu").on("click", function () {
    jQuery(".toggle_menu").toggleClass("mobile-active");
    jQuery(".page_header").toggleClass("mobile-active");
  });

  jQuery(".mainmenu a").on("click", function () {
    if (!jQuery(this).hasClass("sf-with-ul")) {
      jQuery(".toggle_menu").toggleClass("mobile-active");
      jQuery(".page_header").toggleClass("mobile-active");
    }
  });

  //side header processing
  var $sideHeader = jQuery(".page_header_side");
  if ($sideHeader.length) {
    var $body = jQuery("body");
    jQuery(".toggle_menu_side").on("click", function () {
      if (jQuery(this).hasClass("header-slide")) {
        $sideHeader.toggleClass("active-slide-side-header");
      } else {
        if (jQuery(this).parent().hasClass("header_side_right")) {
          $body.toggleClass("active-side-header slide-right");
        } else {
          $body.toggleClass("active-side-header");
        }
      }
    });
    // toggle sub-menus visibility on menu-side-click
    jQuery("ul.menu-side-click")
      .find("li")
      .each(function () {
        var $thisLi = jQuery(this);
        //toggle submenu only for menu items with submenu
        if ($thisLi.find("ul").length) {
          $thisLi
            .append('<span class="activate_submenu"></span>')
            .find(".activate_submenu")
            .on("click", function () {
              var $thisSpan = jQuery(this);
              if ($thisSpan.parent().hasClass("active-submenu")) {
                $thisSpan.parent().removeClass("active-submenu");
                return;
              }
              $thisLi
                .addClass("active-submenu")
                .siblings()
                .removeClass("active-submenu");
            });
        } //eof sumbenu check
      });
    //hidding side header on click outside header
    jQuery("body").on("click", function (e) {
      if (!jQuery(e.target).closest(".page_header_side").length) {
        $sideHeader.removeClass("active-slide-side-header");
        $body.removeClass("active-side-header slide-right");
      }
    });
  } //sideHeader check

  //1 and 2/3/4th level mainmenu offscreen fix
  var MainWindowWidth = jQuery(window).width();
  jQuery(window).on("resize", function () {
    MainWindowWidth = jQuery(window).width();
  });
  //2/3/4 levels
  jQuery(".mainmenu_wrapper .sf-menu")
    .on("mouseover", "ul li", function () {
      // jQuery('.mainmenu').on('mouseover', 'ul li', function(){
      if (MainWindowWidth > 991) {
        var $this = jQuery(this);
        // checks if third level menu exist
        var subMenuExist = $this.find("ul").length;
        if (subMenuExist > 0) {
          var subMenuWidth = $this.find("ul, div").first().width();
          var subMenuOffset =
            $this.find("ul, div").first().parent().offset().left + subMenuWidth;
          // if sub menu is off screen, give new position
          if (subMenuOffset + subMenuWidth > MainWindowWidth) {
            var newSubMenuPosition = subMenuWidth + 0;
            $this.find("ul, div").first().css({
              left: -newSubMenuPosition,
            });
          } else {
            $this.find("ul, div").first().css({
              left: "100%",
            });
          }
        }
      }
      //1st level
    })
    .on("mouseover", "> li", function () {
      if (MainWindowWidth > 991) {
        var $this = jQuery(this);
        var subMenuExist = $this.find("ul").length;
        if (subMenuExist > 0) {
          var subMenuWidth = $this.find("ul").width();
          var subMenuOffset = $this.find("ul").parent().offset().left;
          // if sub menu is off screen, give new position
          if (subMenuOffset + subMenuWidth > MainWindowWidth) {
            var newSubMenuPosition =
              MainWindowWidth - (subMenuOffset + subMenuWidth);
            $this.find("ul").first().css({
              left: newSubMenuPosition,
            });
          }
        }
      }
    });

  /////////////////////////////////////////
  //single page localscroll and scrollspy//
  /////////////////////////////////////////
  var navHeight = jQuery(".page_header").outerHeight(true);
  if (jQuery(".mainmenu_wrapper").length) {
    jQuery("body").scrollspy({
      target: ".mainmenu_wrapper",
      offset: navHeight,
    });
  }
  if (jQuery(".mainmenu_side_wrapper").length) {
    jQuery("body").scrollspy({
      target: ".mainmenu_side_wrapper",
      offset: navHeight,
    });
  }
  if (jQuery().localScroll) {
    jQuery(
      ".mainmenu_wrapper > ul, .mainmenu_side_wrapper > ul, #land"
    ).localScroll({
      duration: 900,
      easing: "easeInOutQuart",
      offset: -navHeight + 10,
    });
  }

  //toTop
  // if (jQuery().UItoTop) {
  //     jQuery().UItoTop({easingType: 'easeOutQuart'});
  // }

  //parallax
  if (jQuery().parallax) {
    jQuery(".parallax").parallax("50%", 0.01);
  }

  //prettyPhoto
  if (jQuery().prettyPhoto) {
    jQuery("a[data-gal^='prettyPhoto']").prettyPhoto({
      hook: "data-gal",
      theme:
        "facebook" /* light_rounded / dark_rounded / light_square / dark_square / facebook / pp_default*/,
    });
  }

  ////////////////////////////////////////
  //init Twitter Bootstrap JS components//
  ////////////////////////////////////////
  //bootstrap carousel
  if (jQuery().carousel) {
    jQuery(".carousel").carousel();
  }
  //bootstrap tab - show first tab
  jQuery(".nav-tabs").each(function () {
    jQuery(this).find("a").first().tab("show");
  });
  jQuery(".tab-content").each(function () {
    jQuery(this).find(".tab-pane").first().addClass("fade in");
  });
  //bootstrap collapse - show first tab
  jQuery(".panel-group").each(function () {
    jQuery(this).find("a").first().filter(".collapsed").trigger("click");
  });
  //tooltip
  if (jQuery().tooltip) {
    jQuery('[data-toggle="tooltip"]').tooltip();
  }

  ////////////////
  //owl carousel//
  ////////////////
  if (jQuery().owlCarousel) {
    jQuery(".owl-carousel").each(function () {
      var $carousel = jQuery(this);
      var loop = $carousel.data("loop") ? $carousel.data("loop") : false;
      var margin =
        $carousel.data("margin") || $carousel.data("margin") == 0
          ? $carousel.data("margin")
          : 30;
      var nav = $carousel.data("nav") ? $carousel.data("nav") : false;
      var dots = $carousel.data("dots") ? $carousel.data("dots") : false;
      var themeClass = $carousel.data("themeclass")
        ? $carousel.data("themeclass")
        : "owl-theme";
      var center = $carousel.data("center") ? $carousel.data("center") : false;
      var items = $carousel.data("items") ? $carousel.data("items") : 4;
      var autoplay = $carousel.data("autoplay")
        ? $carousel.data("autoplay")
        : false;
      var responsiveXs = $carousel.data("responsive-xs")
        ? $carousel.data("responsive-xs")
        : 1;
      var responsiveSm = $carousel.data("responsive-sm")
        ? $carousel.data("responsive-sm")
        : 2;
      var responsiveMd = $carousel.data("responsive-md")
        ? $carousel.data("responsive-md")
        : 3;
      var responsiveLg = $carousel.data("responsive-lg")
        ? $carousel.data("responsive-lg")
        : 4;
      var filters = $carousel.data("filters")
        ? $carousel.data("filters")
        : false;
      var dotsContainer = $carousel.data("dots-container")
        ? $carousel.data("dots-container")
        : false;

      if (filters) {
        $carousel
          .clone()
          .appendTo($carousel.parent())
          .addClass(filters.substring(1) + "-carousel-original");
        jQuery(filters).on("click", "a", function (e) {
          //processing filter link
          e.preventDefault();
          if (jQuery(this).hasClass("selected")) {
            return;
          }
          var filterValue = jQuery(this).attr("data-filter");
          jQuery(this).siblings().removeClass("selected active");
          jQuery(this).addClass("selected active");

          //removing old items
          $carousel.find(".owl-item").length;
          for (var i = $carousel.find(".owl-item").length - 1; i >= 0; i--) {
            $carousel.trigger("remove.owl.carousel", [1]);
          }
          //adding new items
          var $filteredItems = jQuery(
            $carousel
              .next()
              .find(" > " + filterValue)
              .clone()
          );
          $filteredItems.each(function () {
            $carousel.trigger("add.owl.carousel", jQuery(this));
          });

          $carousel.trigger("refresh.owl.carousel");
        });
      } //filters

      $carousel
        .owlCarousel({
          loop: loop,
          margin: margin,
          nav: nav,
          autoplay: autoplay,
          dots: dots,
          themeClass: themeClass,
          center: center,
          items: items,
          responsive: {
            0: {
              items: responsiveXs,
            },
            767: {
              items: responsiveSm,
            },
            992: {
              items: responsiveMd,
            },
            1200: {
              items: responsiveLg,
            },
          },
          dotsContainer: dotsContainer,
        })
        .addClass(themeClass);
      if (center) {
        $carousel.addClass("owl-center");
      }
    });
  } //eof owl-carousel

} //eof documentReadyInit
